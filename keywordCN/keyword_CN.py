#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time   :  2019/7/4 10:50
@Author :  geqh
@file   :  keyword_EN.py
"""
from textrank4zh import TextRank4Keyword
from utils.log import *
from utils.confPaser import *
import os


def keywordCN(text, keyphrasenum=4):
    stoppath = os.path.join(project_path, "conf/stopword_cn.txt")
    word = TextRank4Keyword(stop_words_file=stoppath)
    word.analyze(text, window=2, lower=True)
    w_list = word.get_keywords(num=keyphrasenum, word_min_len=2)
    keyphrase = []
    for w in w_list:
        keyphrase.append({"phrase": w['word'], "weight": w['weight']})
    logger.info(keyphrase)
    return keyphrase

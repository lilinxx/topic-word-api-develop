# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time   :  2019/7/4 10:50
@Author :  geqh
@file   :  api.py
"""
from flask import Flask, request
from keywordCN import keyword_CN
from keywordEN import keyword_EN
from utils.result import *
from utils.log import *
import json

app = Flask(__name__)


# 接口classification
@app.route('/keyPhrase', methods=['GET', 'POST'])
def keyPhrase():
    try:  # 非必要参数
        keyphrasenum = int(request.values['num'])
    except:
        keyphrasenum = 4
    try:  # 必要参数
        text = request.values['text']
        lang = request.values['lang']
    except:
        errMsg = 'parameter [text/lang] is lost'
        res_fail = result(False, errMsg, None)
        res = json.dumps(obj=res_fail.__dict__, ensure_ascii=False)
        logger.error(errMsg)
        return res

    logger.info("parameter text--" + text)
    logger.info("parameter lang--" + lang)

    # 英文
    if lang == 'en':
        res = keyword_EN.keywordEN(text, keyphrasenum)
        res_suc = result(True, None, res)
        res = json.dumps(obj=res_suc.__dict__, ensure_ascii=False)
        return res
    # 中文
    elif lang == 'cn':
        res = keyword_CN.keywordCN(text, keyphrasenum)
        res_suc = result(True, None, res)
        res = json.dumps(obj=res_suc.__dict__, ensure_ascii=False)
        return res
    else:
        res_fail = result(False, 'invalid language', None)
        res = json.dumps(obj=res_fail.__dict__, ensure_ascii=False)
        return res


# 可直接测试
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8018)

# topic-word-api-develop

#### Description
## 基本介绍
```
该工程为关键词提取接口实现，主要用于关键词提取接口发布。
开发环境python3

中文关键词提取：textrank4zh
英文关键词提取：RAKE
```
## 开发原则
```
* 日志规则参见utils/log.py，使用:from utils.log import *
* api.py为接口实现文件，可直接运行测试，内部可指定端口
* utils/confPaser.py 配置文件读取，使用:from utils.confPaser import *
* utils/result.py 返回结果定义
```
## 目录结构
```
├─ keywordCN  # 中文目录
│  └─ keyword_CN.py # 中文实现函数
├─ keywordEN # 英文目录
│  └─ keyword_EN.py  # 英文实现函数
│  └─ RAKE.py  # guanj实现函数
├─ utils # 常用函数定义目录
│  └─ log.py # 日志定义
│  └─ confPaser.py  # 配置文件解析
│  └─ result.py  # 

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

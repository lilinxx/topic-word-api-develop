## 基本介绍
```
该工程为关键词提取接口实现，主要用于关键词提取接口发布。
开发环境python3

中文关键词提取：textrank4zh
英文关键词提取：RAKE
```
## 开发原则
```
* 日志规则参见utils/log.py，使用:from utils.log import *
* api.py为接口实现文件，可直接运行测试，内部可指定端口
* utils/confPaser.py 配置文件读取，使用:from utils.confPaser import *
* utils/result.py 返回结果定义
```
## 目录结构
```
├─ keywordCN  # 中文目录
│  └─ keyword_CN.py # 中文实现函数
├─ keywordEN # 英文目录
│  └─ keyword_EN.py  # 英文实现函数
│  └─ RAKE.py  # guanj实现函数
├─ utils # 常用函数定义目录
│  └─ log.py # 日志定义
│  └─ confPaser.py  # 配置文件解析
│  └─ result.py  # 返回结果定义，方便json格式化
├─ conf 
│  └─ conf.cfg # 配置文件
│  └─ stopword_en.txt # 英文配置文件
│  └─ stopword_cn.txt # 中文配置文件
├─ api.py # 接口实现
├─ README.md
├─ requirements.txt # 依赖包
```
## 启动方法
### 安装配置
```
 建议使用虚拟环境，防止各个工程依赖环境出现版本冲突
```
* windows环境（pycharm）
```
* 安装python3环境，设置环境变量（前提）
* 配置虚拟环境（隔离其他工程）
    File | Settings | Project: xxx | Project Interpreter
    add >> 填写虚拟环境目录（默认工程内部venv目录）>> base Interpreter(已经安装的基础Python环境)
* 安装依赖环境 
    Terminal窗口中 
    pip install -r requirements.txt
```
* linux 环境
```
* 安装python3（前提）
* pip3 install virtualenv
* 在工程目录下，创建虚拟环境env  
    virtualenv env
    
* 启用此环境，后续命令行前面出现（env）代表此时环境已切换，
    source ./env/bin/activate
    
* 之后执行pip python3 等指令，相当于是在此环境中执行
    pip3 install -r requirements.txt
    
* 查看已安装依赖，
    pip3 list
    
* 退出虚拟环境，
    deactivate
    
```
* 启动
```
1. 更改api.py中的端口
2. python3 api.py
```

* 生成 requirements.txt
```
# 每次提交代码工程之前需要重新生成requirements.txt，然后提交
生成方式，Terminal窗口中 
pip freeze > requirements.txt
```

### 接口定义
* 输入

| 参数 | 类型 | 是否必须 |详细说明 |
| ------ | ------ | ------ | ------ |
| text | string | 是 | 待处理文本 |
| lang | string | 是 | 文本语言cn/en |

* 输出

| 参数 | 类型 | 详细说明 |
| ------ | ------ | ------ |
| isSuc | boolean | 是否调用成功 |
| errMsg | string | 接口错误信息 |
| res | string | 类别 |

* 示例
```
http://127.0.0.1:8018/keyPhrase?lang=en&num=6&text=NLTK is a leading platform for building Python programs to work with human language data. It provides easy-to-use interfaces to over 50 corpora and lexical resources such as WordNet, along with a suite of text processing libraries for classification, tokenization, stemming, tagging, parsing, and semantic reasoning, wrappers for industrial-strength NLP libraries, and an active discussion forum.
{
	"isSuc": true,
	"errMsg": null,
	"res": [{
		"phrase": "building python programs",
		"weight": 9.0
	}, {
		"phrase": "human language data",
		"weight": 9.0
	}, {
		"phrase": "text processing libraries",
		"weight": 9.0
	}, {
		"phrase": "industrial-strength nlp libraries",
		"weight": 9.0
	}, {
		"phrase": "active discussion forum",
		"weight": 9.0
	}, {
		"phrase": "leading platform",
		"weight": 4.0
	}]
}

http://127.0.0.1:8017/keyPhrase?lang=cn&num=6&text=我爱北京天安门
{
	"isSuc": true,
	"errMsg": null,
	"res": [{
		"phrase": "北京",
		"weight": 0.48648582432442095
	}, {
		"phrase": "天安门",
		"weight": 0.25675708783778944
	}]
}

```
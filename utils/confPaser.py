#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time         :  2019/7/4 16:38
@Author       :  geqh
@file         :  confPaser.py
@introduction :  配置文件解析
"""

from configparser import ConfigParser
import os

# 初始化类
cur_path = os.path.split(os.path.abspath(__file__))[0]
project_path = os.path.dirname(cur_path)
conf_file = project_path+"/conf/conf.cfg"

if not os.path.exists(conf_file):
    raise RuntimeError(conf_file + ' is not exists')
cp = ConfigParser()
cp.read(conf_file, "utf-8")


#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Time         :  2019/7/15 15:04
@Author       :  geqh
@file         :  result.py
@introduction :  
"""


class result(object):
    def __init__(self, isSuc, errMsg, res):
        self.isSuc = isSuc
        self.errMsg = errMsg
        self.res = res
